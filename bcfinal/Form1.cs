﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lib;

namespace bcfinal
{
    public partial class Form1 : Form

    {
        compteur n;
        public Form1()
        {
            InitializeComponent();
            this.n = new compteur();
        }

        private void plus_Click(object sender, EventArgs e)
        {
            label1.Text = this.n.incrementation();
        }

        private void moins_Click(object sender, EventArgs e)
        {
            label1.Text = this.n.decrementation();
        }

        private void raz_Click(object sender, EventArgs e)
        {
            label1.Text = this.n.raz();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
