﻿namespace bcfinal
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.moins = new System.Windows.Forms.Button();
            this.plus = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.raz = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // moins
            // 
            this.moins.Location = new System.Drawing.Point(104, 110);
            this.moins.Name = "moins";
            this.moins.Size = new System.Drawing.Size(75, 23);
            this.moins.TabIndex = 0;
            this.moins.Text = "-";
            this.moins.UseVisualStyleBackColor = true;
            this.moins.Click += new System.EventHandler(this.moins_Click);
            // 
            // plus
            // 
            this.plus.Location = new System.Drawing.Point(391, 109);
            this.plus.Name = "plus";
            this.plus.Size = new System.Drawing.Size(75, 23);
            this.plus.TabIndex = 1;
            this.plus.Text = "+";
            this.plus.UseVisualStyleBackColor = true;
            this.plus.Click += new System.EventHandler(this.plus_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(266, 119);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(269, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "resultat";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // raz
            // 
            this.raz.Location = new System.Drawing.Point(225, 181);
            this.raz.Name = "raz";
            this.raz.Size = new System.Drawing.Size(75, 23);
            this.raz.TabIndex = 4;
            this.raz.Text = "RAZ";
            this.raz.UseVisualStyleBackColor = true;
            this.raz.Click += new System.EventHandler(this.raz_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 262);
            this.Controls.Add(this.raz);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.plus);
            this.Controls.Add(this.moins);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button moins;
        private System.Windows.Forms.Button plus;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button raz;
    }
}

