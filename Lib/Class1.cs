﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib
{
    public class compteur
    {
        private int cptr;

        public compteur()
        {
            cptr = 0;
        }
        public string incrementation()
        {
            cptr++;
            return string.Format("{0}", cptr);
        }
        public string decrementation()
        {
            if (this.getcptr() > 0)
            { cptr--; }
            return string.Format("{0}", cptr);
        }
        public string raz()
        {
            cptr = 0;
            return string.Format("{0}", cptr);
        }

        public int getcptr()
        {
            return this.cptr;
        }

        public void setcptr(int n)
        {
            this.cptr = n;
        }
    }
}
